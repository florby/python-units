#!/bin/env python3
# -*- coding: utf-8 -*-
#
# Part Python Units, a small library for parsing and converting various units.
#
# MIT License

# Copyright (c) 2018 Peter Gebauer

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


from units import get_context, unit
from units import si, imperial, computer


if __name__ == "__main__":
    # Register unit types, a list of types or modules with unit types
    get_context().append(si, computer, imperial.UnitImperialPound, imperial.UnitImperialFahrenheit)
    # Convert easily
    print("Volume conversion: ", unit("2m³").convert("l"))
    print("Temperature conversion: ", unit("70F").convert("C"))
    # Automatic conversion between compatible units during arithmetic operations
    print("Data addition:", unit("1024kB") + unit("8Mb"))
    print("Mass subtraction:", unit("10kg") - unit("2.5lbs"))
    # Multiplication is allowed in certain cases, like m * m = m²
    print("Length multiplication:", unit("2m") * unit("3m"))
    # Likewise with division, here we are taking one side away from a cube
    print("Volume division:", unit("4m³") / unit("2m"))
    # Coercing between incompatible units using a factor like density
    print("Volume to mass for oil:", unit("kg").coerce("10l", 900))
