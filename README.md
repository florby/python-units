# Python Units

A small library to parse and convert various units. MIT license.

NOTE it is still early in development, the conversions may be incorrect and/or contain bugs.

# Example

```python
from units import get_context, unit
from units import si, imperial, computer


if __name__ == "__main__":
    # Register unit types, a list of types or modules with unit types
    get_context().append(si, computer, imperial.UnitImperialPound, imperial.UnitImperialFahrenheit)
    # Convert easily
    print("Volume conversion: ", unit("2m³").convert("l"))
    print("Temperature conversion: ", unit("70F").convert("C"))
    # Automatic conversion between compatible units during arithmetic operations
    print("Data addition:", unit("1024kB") + unit("8Mb"))
    print("Mass subtraction:", unit("10kg") - unit("2.5lbs"))
    # Multiplication is allowed in certain cases, like m * m = m²
    print("Length multiplication:", unit("2m") * unit("3m"))
    # Coercing between incompatible units using a factor like density
    print("Volume to mass for oil:", unit("kg").coerce("10l", 900))
```

Output:
```
Volume conversion:  2000l
Temperature conversion:  21.11111111111111111111111111°C
Data addition: 2048kB
Mass subtraction: 8.866019075kg
Length multiplication: 6m²
Volume to mass for oil: 9kg
```


# Installation

Still early in development, if you want to contribute with an installer that's fine.