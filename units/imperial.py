# -*- coding: utf-8 -*-
#
# Imperial units (and friends).
#
# Part Python Units, a small library for parsing and converting various units.
#
# MIT License

# Copyright (c) 2018 Peter Gebauer

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import decimal
from units import *
from units import si


class UnitImperial(Unit):
    """
    The base for all imperial  units.
    """

    SYSTEM = "imperial-us"
    SYMBOL_POSITION = SYMBOL_POSITION_SUFFIX
    
    
    def convert_value_to_instance(self, instance):
        if type(instance) is UnitImperial:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitImperial:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        return super().convert_value_from_instance(instance)


class UnitImperialThou(UnitImperial):
    """
    Imperial  thou.
    """

    MEASURE = "length"
    SYMBOLS = ("th", )
    NAMES = ("thou", "thou")


    def convert_value_to_instance(self, instance):
        if type(instance) is UnitImperialThou:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is si.UnitSIMetre:
            return (self.value * self.multiplier_value * decimal.Decimal("0.0000254")) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitImperialThou:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is si.UnitSIMetre:
            return (instance.value * instance.multiplier_value / decimal.Decimal("0.0000254")) / self.multiplier_value
        return super().convert_value_from_instance(instance)

    


class UnitImperialInch(UnitImperial):
    """
    Imperial  inch.
    """

    MEASURE = "length"
    SYMBOLS = ("\"", "in",)
    NAMES = ("inch", "inches")
    
    
    def convert_value_to_instance(self, instance):
        if type(instance) is UnitImperialInch:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is UnitImperialThou:
            return (self.value * self.multiplier_value * 1000) / instance.multiplier_value
        elif type(instance) is si.UnitSIMetre:
            return (self.value * self.multiplier_value * decimal.Decimal("0.0254")) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitImperialInch:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is UnitImperialThou:
            return (instance.value * instance.multiplier_value / 1000) / self.multiplier_value
        elif type(instance) is si.UnitSIMetre:
            return (instance.value * instance.multiplier_value / decimal.Decimal("0.0254")) / self.multiplier_value
        return super().convert_value_from_instance(instance)


class UnitImperialFoot(UnitImperial):
    """
    Imperial  foot.
    """

    MEASURE = "length"
    SYMBOLS = ("'", "ft",)
    NAMES = ("foot", "feet")

    
    def convert_value_to_instance(self, instance):
        if type(instance) is UnitImperialFoot:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is UnitImperialInch:
            return (self.value * self.multiplier_value * 12) / instance.multiplier_value
        elif type(instance) is UnitImperialThou:
            return (self.value * self.multiplier_value * 12000) / instance.multiplier_value
        elif type(instance) is si.UnitSIMetre:
            return (self.value * self.multiplier_value * decimal.Decimal("0.3040")) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitImperialFoot:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is UnitImperialInch:
            return (instance.value * instance.multiplier_value / 12) / self.multiplier_value
        elif type(instance) is UnitImperialThou:
            return (instance.value * instance.multiplier_value / 12000) / self.multiplier_value
        elif type(instance) is si.UnitSIMetre:
            return (instance.value * instance.multiplier_value / decimal.Decimal("0.3040")) / self.multiplier_value
        return super().convert_value_from_instance(instance)
    

class UnitImperialYard(UnitImperial):
    """
    Imperial  yard.
    """

    MEASURE = "length"
    SYMBOLS = ("yd",)
    NAMES = ("yard", "yards")
    
    
    # def get_converted_value_impl(self, instance):
    #     if type(instance) is UnitImperialYard:
    #         return self.value
    #     elif type(instance) is UnitImperialFoot:
    #         return self.value * decimal.Decimal("3")
    #     elif type(instance) is UnitImperialInch:
    #         return self.value * decimal.Decimal("36")
    #     elif type(instance) is UnitImperialThou:
    #         return self.value * decimal.Decimal("36000")
    #     elif type(instance) is si.UnitSIMetre:
    #         return (self.value * decimal.Decimal("0.9144")) / instance.multiplier
    #     elif type(instance) is si.UnitImperial:
    #         return self.value
    #     return None
        

    # def convert_value_from_instance_impl(self, instance):
    #     if type(instance) is UnitImperialYard:
    #         return instance.value
    #     elif type(instance) is UnitImperialFoot:
    #         return instance.value / decimal.Decimal("3")
    #     elif type(instance) is UnitImperialInch:
    #         return instance.value / decimal.Decimal("36")
    #     elif type(instance) is UnitImperialThou:
    #         return instance.value / decimal.Decimal("36000")
    #     elif type(instance) is si.UnitSIMetre:
    #         return instance.multiplied_value * decimal.Decimal("0.9144")
    #     elif type(instance) is si.UnitImperial:
    #         return instance.value
    #     return None


    def convert_value_to_instance(self, instance):
        if type(instance) is UnitImperialYard:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is UnitImperialFoot:
            return (self.value * self.multiplier_value * 3) / instance.multiplier_value
        elif type(instance) is UnitImperialInch:
            return (self.value * self.multiplier_value * 36) / instance.multiplier_value
        elif type(instance) is UnitImperialThou:
            return (self.value * self.multiplier_value * 36000) / instance.multiplier_value
        elif type(instance) is si.UnitSIMetre:
            return (self.value * self.multiplier_value * decimal.Decimal("0.9144")) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitImperialYard:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is UnitImperialFoot:
            return (instance.value * instance.multiplier_value / 3) / self.multiplier_value
        elif type(instance) is UnitImperialInch:
            return (instance.value * instance.multiplier_value / 36) / self.multiplier_value
        elif type(instance) is UnitImperialThou:
            return (instance.value * instance.multiplier_value / 36000) / self.multiplier_value
        elif type(instance) is si.UnitSIMetre:
            return (instance.value * instance.multiplier_value / decimal.Decimal("0.9144")) / self.multiplier_value
        return super().convert_value_from_instance(instance)
    

class UnitImperialFurlong(UnitImperial):
    """
    Imperial  furlong.
    """

    MEASURE = "length"
    SYMBOLS = ("fur",)
    NAMES = ("furlong", "furlongs")
    

    def convert_value_to_instance(self, instance):
        if type(instance) is UnitImperialFurlong:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is UnitImperialYard:
            return (self.value * self.multiplier_value * 220) / instance.multiplier_value
        elif type(instance) is UnitImperialFoot:
            return (self.value * self.multiplier_value * 660) / instance.multiplier_value
        elif type(instance) is UnitImperialInch:
            return (self.value * self.multiplier_value * 7920) / instance.multiplier_value
        elif type(instance) is UnitImperialThou:
            return (self.value * self.multiplier_value * 7920000) / instance.multiplier_value
        elif type(instance) is si.UnitSIMetre:
            return (self.value * self.multiplier_value * decimal.Decimal("201.168")) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitImperialFurlong:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is UnitImperialYard:
            return (instance.value * instance.multiplier_value / 220) / self.multiplier_value
        elif type(instance) is UnitImperialFoot:
            return (instance.value * instance.multiplier_value / 660) / self.multiplier_value
        elif type(instance) is UnitImperialInch:
            return (instance.value * instance.multiplier_value / 7920) / self.multiplier_value
        elif type(instance) is UnitImperialThou:
            return (instance.value * instance.multiplier_value / 7920000) / self.multiplier_value
        elif type(instance) is si.UnitSIMetre:
            return (instance.value * instance.multiplier_value / decimal.Decimal("201.168")) / self.multiplier_value
        return super().convert_value_from_instance(instance)
    

class UnitImperialMile(UnitImperial):
    """
    Imperial  mile.
    """

    MEASURE = "length"
    SYMBOLS = ("mi",)
    NAMES = ("mile", "miles")


    def convert_value_to_instance(self, instance):
        if type(instance) is UnitImperialMile:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is UnitImperialFurlong:
            return (self.value * self.multiplier_value * 8) / instance.multiplier_value
        elif type(instance) is UnitImperialYard:
            return (self.value * self.multiplier_value * 1760) / instance.multiplier_value
        elif type(instance) is UnitImperialFoot:
            return (self.value * self.multiplier_value * 5280) / instance.multiplier_value
        elif type(instance) is UnitImperialInch:
            return (self.value * self.multiplier_value * 63360) / instance.multiplier_value
        elif type(instance) is UnitImperialThou:
            return (self.value * self.multiplier_value * 63360000) / instance.multiplier_value
        elif type(instance) is si.UnitSIMetre:
            return (self.value * self.multiplier_value * decimal.Decimal("1609.344")) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitImperialMile:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is UnitImperialFurlong:
            return (instance.value * instance.multiplier_value / 8) / self.multiplier_value
        elif type(instance) is UnitImperialYard:
            return (instance.value * instance.multiplier_value / 1760) / self.multiplier_value
        elif type(instance) is UnitImperialFoot:
            return (instance.value * instance.multiplier_value / 5280) / self.multiplier_value
        elif type(instance) is UnitImperialInch:
            return (instance.value * instance.multiplier_value / 63360) / self.multiplier_value
        elif type(instance) is UnitImperialThou:
            return (instance.value * instance.multiplier_value / 63360000) / self.multiplier_value
        elif type(instance) is si.UnitSIMetre:
            return (instance.value * instance.multiplier_value / decimal.Decimal("1609.344")) / self.multiplier_value
        return super().convert_value_from_instance(instance)


class UnitImperialFluidOunce(UnitImperial):
    """
    Imperial  fluid ounce.
    """

    MEASURE = "volume"
    SYMBOLS = ("floz", "fl oz")
    NAMES = ("fluid ounce", "fluid ounces")


    def convert_value_to_instance(self, instance):
        if type(instance) is UnitImperialFluidOunce:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is si.UnitSILitre:
            return (self.value * self.multiplier_value * decimal.Decimal("0.0284130625")) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitImperialFluidOunce:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is si.UnitSILitre:
            return (instance.value * instance.multiplier_value / decimal.Decimal("0.0284130625")) / self.multiplier_value
        return super().convert_value_from_instance(instance)


class UnitImperialGill(UnitImperial):
    """
    Imperial  gill.
    """

    MEASURE = "volume"
    SYMBOLS = ("gi",)
    NAMES = ("gill", "gills")


    def convert_value_to_instance(self, instance):
        if type(instance) is UnitImperialGill:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is UnitImperialFluidOunce:
            return (self.value * self.multiplier_value * 5) / instance.multiplier_value
        elif type(instance) is si.UnitSILitre:
            return (self.value * self.multiplier_value * decimal.Decimal("0.1420653125")) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitImperialGill:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is UnitImperialFluidOunce:
            return (instance.value * instance.multiplier_value / 5) / self.multiplier_value
        elif type(instance) is si.UnitSILitre:
            return (instance.value * instance.multiplier_value / decimal.Decimal("0.1420653125")) / self.multiplier_value
        return super().convert_value_from_instance(instance)
    

class UnitImperialPint(UnitImperial):
    """
    Imperial  pint.
    """

    MEASURE = "volume"
    SYMBOLS = ("pt",)
    NAMES = ("pint", "pints")


    def convert_value_to_instance(self, instance):
        if type(instance) is UnitImperialPint:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is UnitImperialGill:
            return (self.value * self.multiplier_value * 4) / instance.multiplier_value
        elif type(instance) is UnitImperialFluidOunce:
            return (self.value * self.multiplier_value * 20) / instance.multiplier_value
        elif type(instance) is si.UnitSILitre:
            return (self.value * self.multiplier_value * decimal.Decimal("0.56826125")) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitImperialPint:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is UnitImperialGill:
            return (instance.value * instance.multiplier_value / 4) / self.multiplier_value
        elif type(instance) is UnitImperialFluidOunce:
            return (instance.value * instance.multiplier_value / 20) / self.multiplier_value
        elif type(instance) is si.UnitSILitre:
            return (instance.value * instance.multiplier_value / decimal.Decimal("0.56826125")) / self.multiplier_value
        return super().convert_value_from_instance(instance)


class UnitImperialQuart(UnitImperial):
    """
    Imperial  quart.
    """

    MEASURE = "volume"
    SYMBOLS = ("qt",)
    NAMES = ("quart", "quarts")


    def convert_value_to_instance(self, instance):
        if type(instance) is UnitImperialQuart:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is UnitImperialPint:
            return (self.value * self.multiplier_value * 2) / instance.multiplier_value
        elif type(instance) is UnitImperialGill:
            return (self.value * self.multiplier_value * 8) / instance.multiplier_value
        elif type(instance) is UnitImperialFluidOunce:
            return (self.value * self.multiplier_value * 40) / instance.multiplier_value
        elif type(instance) is si.UnitSILitre:
            return (self.value * self.multiplier_value * decimal.Decimal("1.1365225")) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitImperialQuart:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is UnitImperialPint:
            return (instance.value * instance.multiplier_value / 2) / self.multiplier_value
        elif type(instance) is UnitImperialGill:
            return (instance.value * instance.multiplier_value / 8) / self.multiplier_value
        elif type(instance) is UnitImperialFluidOunce:
            return (instance.value * instance.multiplier_value / 40) / self.multiplier_value
        elif type(instance) is si.UnitSILitre:
            return (instance.value * instance.multiplier_value / decimal.Decimal("1.1365225")) / self.multiplier_value
        return super().convert_value_from_instance(instance)


class UnitImperialGallon(UnitImperial):
    """
    Imperial  gallon.
    """

    MEASURE = "volume"
    SYMBOLS = ("gal",)
    NAMES = ("gallon", "gallons")


    def convert_value_to_instance(self, instance):
        if type(instance) is UnitImperialGallon:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is UnitImperialQuart:
            return (self.value * self.multiplier_value * 4) / instance.multiplier_value
        elif type(instance) is UnitImperialPint:
            return (self.value * self.multiplier_value * 8) / instance.multiplier_value
        elif type(instance) is UnitImperialGill:
            return (self.value * self.multiplier_value * 32) / instance.multiplier_value
        elif type(instance) is UnitImperialFluidOunce:
            return (self.value * self.multiplier_value * 160) / instance.multiplier_value
        elif type(instance) is si.UnitSILitre:
            return (self.value * self.multiplier_value * decimal.Decimal("4.54609")) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitImperialGallon:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is UnitImperialQuart:
            return (instance.value * instance.multiplier_value / 4) / self.multiplier_value
        elif type(instance) is UnitImperialPint:
            return (instance.value * instance.multiplier_value / 8) / self.multiplier_value
        elif type(instance) is UnitImperialGill:
            return (instance.value * instance.multiplier_value / 32) / self.multiplier_value
        elif type(instance) is UnitImperialFluidOunce:
            return (instance.value * instance.multiplier_value / 160) / self.multiplier_value
        elif type(instance) is si.UnitSILitre:
            return (instance.value * instance.multiplier_value / decimal.Decimal("4.54609")) / self.multiplier_value
        return super().convert_value_from_instance(instance)


class UnitImperialGrain(UnitImperial):
    """
    Imperial  grain.
    """

    MEASURE = "mass"
    SYMBOLS = ("gr",)
    NAMES = ("grain", "grains")


    def convert_value_to_instance(self, instance):
        if type(instance) is UnitImperialGrain:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is si.UnitSIGram:
            return (self.value * self.multiplier_value * decimal.Decimal("0.06479891")) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitImperialGrain:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is si.UnitSIGram:
            return (instance.value * instance.multiplier_value / decimal.Decimal("0.06479891")) / self.multiplier_value
        return super().convert_value_from_instance(instance)


class UnitImperialOunce(UnitImperial):
    """
    Imperial  grain.
    """

    MEASURE = "mass"
    SYMBOLS = ("oz",)
    NAMES = ("ounce", "ounces")


    def convert_value_to_instance(self, instance):
        if type(instance) is UnitImperialOunce:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is UnitImperialGrain:
            return (self.value * self.multiplier_value * decimal.Decimal("437.5")) / instance.multiplier_value
        elif type(instance) is si.UnitSIGram:
            return (self.value * self.multiplier_value * decimal.Decimal("0.06479891")) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitImperialOunce:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is UnitImperialGrain:
            return (instance.value * instance.multiplier_value / decimal.Decimal("437.5")) / self.multiplier_value
        elif type(instance) is si.UnitSIGram:
            return (instance.value * instance.multiplier_value / decimal.Decimal("0.06479891")) / self.multiplier_value
        return super().convert_value_from_instance(instance)


class UnitImperialPound(UnitImperial):
    """
    Imperial  pound.
    """

    MEASURE = "mass"
    SYMBOLS = ("lb", "lbs")
    NAMES = ("pound", "pounds")


    def convert_value_to_instance(self, instance):
        if type(instance) is UnitImperialPound:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is UnitImperialOunce:
            return (self.value * self.multiplier_value * 16) / instance.multiplier_value
        elif type(instance) is UnitImperialGrain:
            return (self.value * self.multiplier_value * 7000) / instance.multiplier_value
        elif type(instance) is si.UnitSIGram:
            return (self.value * self.multiplier_value * decimal.Decimal("453.59237")) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitImperialPound:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is UnitImperialOunce:
            return (instance.value * instance.multiplier_value / 16) / self.multiplier_value
        elif type(instance) is UnitImperialGrain:
            return (instance.value * instance.multiplier_value / 7000) / self.multiplier_value
        elif type(instance) is si.UnitSIGram:
            return (instance.value * instance.multiplier_value / decimal.Decimal("453.59237")) / self.multiplier_value
        return super().convert_value_from_instance(instance)


class UnitImperialStone(UnitImperial):
    """
    Imperial  stone.
    """

    MEASURE = "mass"
    SYMBOLS = ("st",)
    NAMES = ("stone", "stones")


    def convert_value_to_instance(self, instance):
        if type(instance) is UnitImperialStone:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is UnitImperialPound:
            return (self.value * self.multiplier_value * 14) / instance.multiplier_value
        elif type(instance) is UnitImperialOunce:
            return (self.value * self.multiplier_value * 224) / instance.multiplier_value
        elif type(instance) is UnitImperialGrain:
            return (self.value * self.multiplier_value * 98000) / instance.multiplier_value
        elif type(instance) is si.UnitSIGram:
            return (self.value * self.multiplier_value * decimal.Decimal("6350.29318")) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitImperialStone:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is UnitImperialPound:
            return (instance.value * instance.multiplier_value / 14) / self.multiplier_value
        elif type(instance) is UnitImperialOunce:
            return (instance.value * instance.multiplier_value / 224) / self.multiplier_value
        elif type(instance) is UnitImperialGrain:
            return (instance.value * instance.multiplier_value / 98000) / self.multiplier_value
        elif type(instance) is si.UnitSIGram:
            return (instance.value * instance.multiplier_value / decimal.Decimal("6350.29318")) / self.multiplier_value
        return super().convert_value_from_instance(instance)


class UnitImperialTon(UnitImperial):
    """
    Imperial ton.
    """

    MEASURE = "mass"
    SYMBOLS = ("t",)
    NAMES = ("ton", "tons")


    def convert_value_to_instance(self, instance):
        if type(instance) is UnitImperialTon:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is UnitImperialStone:
            return (self.value * self.multiplier_value * 160) / instance.multiplier_value
        elif type(instance) is UnitImperialPound:
            return (self.value * self.multiplier_value * 2240) / instance.multiplier_value
        elif type(instance) is UnitImperialOunce:
            return (self.value * self.multiplier_value * 35840) / instance.multiplier_value
        elif type(instance) is UnitImperialGrain:
            return (self.value * self.multiplier_value * 15680000) / instance.multiplier_value
        elif type(instance) is si.UnitSIGram:
            return (self.value * self.multiplier_value * decimal.Decimal("1016046.9088")) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitImperialTon:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is UnitImperialStone:
            return (instance.value * instance.multiplier_value / 160) / self.multiplier_value
        elif type(instance) is UnitImperialPound:
            return (instance.value * instance.multiplier_value / 2240) / self.multiplier_value
        elif type(instance) is UnitImperialOunce:
            return (instance.value * instance.multiplier_value / 35840) / self.multiplier_value
        elif type(instance) is UnitImperialGrain:
            return (instance.value * instance.multiplier_value / 15680000) / self.multiplier_value
        elif type(instance) is si.UnitSIGram:
            return (instance.value * instance.multiplier_value / decimal.Decimal("1016046.9088")) / self.multiplier_value
        return super().convert_value_from_instance(instance)


class UnitImperialFahrenheit(UnitImperial):
    """
    Fahrenheit (friend of imperial system).
    """

    MEASURE = "temperature"
    SYMBOLS = ("°F", "F")
    NAMES = ("fahrenheit", "fahrenheit")


    def convert_value_to_instance(self, instance):
        if type(instance) is UnitImperialFahrenheit:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is si.UnitSIKelvin:
            return ((self.value * self.multiplier_value - decimal.Decimal("459.67")) * (decimal.Decimal("5") / decimal.Decimal("9"))) / instance.multiplier_value
        elif type(instance) is si.UnitSICelsius:
            return ((self.value * self.multiplier_value - 32) * (decimal.Decimal("5") / decimal.Decimal("9"))) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitImperialFahrenheit:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is si.UnitSIKelvin:
            return (instance.value * instance.multiplier_value * (decimal.Decimal("9") / decimal.Decimal("5")) + decimal.Decimal("459.67")) / self.multiplier_value
        elif type(instance) is si.UnitSICelsius:
            return (instance.value * instance.multiplier_value * (decimal.Decimal("9") / decimal.Decimal("5")) + 32) / self.multiplier_value
        return super().convert_value_from_instance(instance)


UNITS = [UnitImperialThou, UnitImperialInch, UnitImperialFoot, UnitImperialYard, UnitImperialFurlong, UnitImperialMile, UnitImperialFluidOunce, UnitImperialGill, UnitImperialPint, UnitImperialQuart, UnitImperialGallon, UnitImperialGrain, UnitImperialOunce, UnitImperialPound, UnitImperialStone, UnitImperialTon, UnitImperialFahrenheit]
