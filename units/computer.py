# -*- coding: utf-8 -*-
#
# Computer related units.
#
# Part Python Units, a small library for parsing and converting various units.
#
# MIT License

# Copyright (c) 2018 Peter Gebauer

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import decimal
from units import *


class UnitData(Unit):
    """
    Base for data.
    """

    MEASURE = "data"
    SYMBOL_POSITION = SYMBOL_POSITION_SUFFIX
    MULTIPLIERS = (
        UnitMultiplier("Y", "yotta", 1024 ** 8),
        UnitMultiplier("Z", "zetta", 1024 ** 7),
        UnitMultiplier("E", "exa", 1024 ** 6),
        UnitMultiplier("P", "peta", 1024 ** 5),
        UnitMultiplier("T", "tera", 1024 ** 4),
        UnitMultiplier("G", "giga", 1024 ** 3),
        UnitMultiplier("M", "mega", 1024 ** 2),
        UnitMultiplier("k", "kilo", 1024)
    )


class UnitDataBit(UnitData):
    """
    A simple bit.
    """

    SYMBOLS = ("b",)
    NAMES = ("bit", "bits")

    
    def convert_value_to_instance(self, instance):
        if type(instance) is UnitDataBit:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitDataBit:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        return super().convert_value_from_instance(instance)


class UnitDataByte(UnitData):
    """
    A byte (octet, 8 bits).
    """

    SYMBOLS = ("B",)
    NAMES = ("byte", "bytes")

    def convert_value_to_instance(self, instance):
        if type(instance) is UnitDataByte:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is UnitDataBit:
            return (self.value * self.multiplier_value * 8) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitDataByte:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is UnitDataBit:
            return (instance.value * instance.multiplier_value / 8) / self.multiplier_value
        return super().convert_value_from_instance(instance)



UNITS = [UnitDataBit, UnitDataByte]
