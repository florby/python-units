# -*- coding: utf-8 -*-
#
# Main module for unit features and home of the default context.
#
# Part Python Units, a small library for parsing and converting various units.
#
# MIT License

# Copyright (c) 2018 Peter Gebauer

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import decimal
import itertools
import types
import re
import locale


SYMBOL_POSITION_BOTH = 0
SYMBOL_POSITION_PREFIX = 1
SYMBOL_POSITION_SUFFIX = 2


def to_decimal(s, default=None):
    """
    Parses a string with decimal notation.
    """
    if s == "" or s is None:
        return decimal.Decimal(0)
    if isinstance(s, str):
        s = s.strip()
    try:
        return decimal.Decimal(s)
    except (decimal.InvalidOperation, ValueError, TypeError):
        pass
    return default


def format_decimal(d, allow_scientific=True):
    """
    Format a decimal to a string.
    """
    if allow_scientific:
        return "%s"%d
    ret = []
    t = d.as_tuple()
    if t.exponent < 0 and abs(t.exponent) > len(t.digits):
        ret.append("0.")
        for i in range(abs(t.exponent) - len(t.digits)):
            ret.append("0")
    for i, d in enumerate(t.digits):
        if t.exponent < 0 and i == len(t.digits) + t.exponent:
            ret.append(".")
        ret.append("%d"%d)
    if t.exponent > 0:
        ret.append("0" * t.exponent)
    return "".join(ret)
    

def to_integer(s, default=None):
    """
    Parses a string with integer notation.
    """
    try:
        return int(s)
    except (ValueError, TypeError):
        pass
    return default


def is_integer(s):
    """
    Check if the string is a valid integer.
    """
    return not to_integer(s, None) is None


class UnitMatch:

    def __init__(self, source, match, number, prefix, suffix, multiplier, start, end):
        self.source = "" if source is None else str(source)
        self.match = "" if match is None else str(match)
        self.number = "" if number is None else str(number)
        self.prefix = "" if prefix is None else str(prefix)
        self.suffix = "" if suffix is None else str(suffix)
        self.multiplier = "" if multiplier is None else str(multiplier)
        self.start = int(start)
        self.end = int(end)

    def __repr__(self):
        return "<%s %d-%d '%s' n='%s'%s%s%s>"%(
            type(self).__name__,
            self.start,
            self.end,
            self.match,
            self.number,
            " prefix='%s'"%self.prefix if self.prefix else "",
            " suffix='%s'"%self.suffix if self.suffix else "",
            " mul='%s'"%self.multiplier if self.multiplier else "")


class UnitMultiplier:
    """
    Used for unit multiplying (such as kilo or mega).
    """

    def __init__(self, symbols, names = None, value = None):
        if isinstance(symbols, UnitMultiplier):
            names = symbols.names
            value = symbols.value
            symbols = symbols.symbols
        if isinstance(symbols, str):
            self.symbols = symbols.split("|")
        elif isinstance(symbols, (list, tuple)):
            self.symbols = list(symbols)
        elif symbols is None:
            self.symbols = []
        else:
            raise TypeError("invalid type for symbols: %s"%type(symbols).__name__)
        if isinstance(names, str):
            self.names = names.split("|")
        elif isinstance(names, (list, tuple)):
            self.names = list(names)
        elif names is None:
            self.names = []
        else:
            raise TypeError("invalid type for names: %s"%type(names).__name__)
        self.value = to_decimal(value)
        if self.value is None:
            raise ValueError("invalid multiplier value: %s"%type(value).__name__)


    def __str__(self):
        return "<%s '%s' '%s' %s>"%(type(self).__name__, "|".join(self.symbols), "|".join(self.names), self.value)
   

    def __eq__(self, other):
        if not isinstance(other, UnitMultiplier):
            raise TypeError("unsupported operand type(s) for ==: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        return bool("".join(self.symbols) == "".join(other.symbols) and "".join(self.names) == "".join(other.names) and self.value == other.value)

    
class Unit:
    """
    Represents any unit, integers and decimals (and strings with integer or decimal notation).
    """

    SYSTEM = ""
    """
    Just a name for the system, preferred lowercase and underscores.
    """

    MEASURE = ""
    """
    What the unit is measuring (i.e "length", "mass", etc).
    """

    SYMBOLS = tuple()
    """
    A tuple with all valid symbols.
    """

    NAMES = tuple()
    """
    A tuple with all valid names.
    """

    MULTIPLIERS = tuple()
    """
    Used to for multipliers such as kilo or milli.
    Multipliers always come AFTER the number.
    """

    SHOW_SCIENTIFIC_NOTATION = False
    """
    True to allow scientific notation when converting a unit to a string (it's always allowed when initializing the value).
    """

    SYMBOL_POSITION = SYMBOL_POSITION_BOTH
    """
    See global variables: SYMBOL_POSITION_BOTH, SYMBOL_POSITION_PREFIX and SYMBOL_POSITION_SUFFIX
    """


    @classmethod
    def assert_type(cls, something):
        if isinstance(something, Unit):
            return type(something)
        if isinstance(something, type) and issubclass(something, Unit):
            return something
        raise AssertionError("%s"%something.__name__ if isinstance(something, type) else type(something).__name__)


    @classmethod
    def compile_regexp(cls):
        symbols_str = "(%s)"%"|".join((re.escape(r) for r in cls.SYMBOLS)) if cls.SYMBOLS else ""
        names_str = "(?i:%s)"%"|".join((re.escape(r) for r in cls.NAMES)) if cls.NAMES else ""
        symbols_and_names_pattern = "(%s)"%"|".join((pattern for pattern in (symbols_str, names_str) if pattern))
        multiplier_symbols = []
        multiplier_names = []
        for multiplier in cls.MULTIPLIERS:
            multiplier_symbols += multiplier.symbols
            multiplier_names += multiplier.names
        multiplier_symbols_str = "(%s)"%"|".join(re.escape(r) for r in multiplier_symbols) if multiplier_symbols else ""
        multiplier_names_str = "(?i:%s)"%"|".join(re.escape(r) for r in multiplier_names) if multiplier_names else ""
        multiplier_pattern = "(%s)"%"|".join((pattern for pattern in (multiplier_symbols_str, multiplier_names_str) if pattern))
        if symbols_and_names_pattern and multiplier_pattern:
            multiplier_pattern += "(?=%s)"%symbols_and_names_pattern
        pattern = "(^|\s)(?P<sign>[\+\-])?%s(?P<number>((\d+\.\d*)|(\.\d+)|\d+)([Ee][+-]?\d+)?)?%s%s(?=\s|$)"%(
            "(?P<prefix>%s)?"%symbols_and_names_pattern if cls.SYMBOL_POSITION != SYMBOL_POSITION_SUFFIX and symbols_and_names_pattern else "",
            "(?P<multiplier>%s)?"%multiplier_pattern if multiplier_pattern else "",
            "(?P<suffix>%s)?"%symbols_and_names_pattern if cls.SYMBOL_POSITION != SYMBOL_POSITION_PREFIX and symbols_and_names_pattern else ""
        )
        return re.compile(pattern)
    

    @classmethod
    def match_string(cls, s):
        if not "_REGEXP" in cls.__dict__:
            cls._REGEXP = cls.compile_regexp()
        if cls._REGEXP:
            m = cls._REGEXP.search(s)
            if m and m.end() > m.start():
                d = m.groupdict()
                sign = d.get("sign")
                if sign is None:
                    sign = ""
                num = d.get("number")
                if num is None:
                    num = ""
                return UnitMatch(s, m.group(), sign + num, d.get("prefix"), d.get("suffix"), d.get("multiplier"), m.start(), m.end())
        return None


    @classmethod
    def find_multiplier(cls, s):
        if isinstance(s, UnitMultiplier):
            for i, sp in enumerate(cls.MULTIPLIERS):
                if s == sp:
                    return i
            return -1
        v = to_decimal(s)
        if v is None and isinstance(s, str):
            sl = s.lower()
            for i, sp in enumerate(cls.MULTIPLIERS):
                if s in sp.symbols or sl in sp.names:
                    return i
            return -1
        if v is None:
            raise TypeError("invalid symbol prefix type: %s"%type(s).__name__)
        for i, sp in enumerate(cls.MULTIPLIERS):
            if v == sp.value:
                return i
        return -1


    def __init__(self, value=None, context=None):
        """
        Initialize the unit. The value can be integer, float, decimal, a unit instance or a string representing any of aforementioned types.
        """
        self._context = context
        self._value = decimal.Decimal(0)
        self._match = None
        self._multiplier = None
        if not self._context and isinstance(value, Unit):
            self._context = value.context
        if type(value) == type(self):
            self._multiplier = value._multiplier
        v = 0 if value is None else value
        if isinstance(value, str):
            m = self.match_string(value.strip())
            if m:
                v = to_decimal(self.handle_match(m))
                if v is None:
                    raise RuntimeError("%s.handle_match() returned non-decimal value from %s"%(type(self).__name__, value))
            else:
                v = to_decimal(value)
        v = self.convert_value_from_instance(v)
        if v is None:
            raise ValueError("invalid value for %s: %s"%(type(self).__name__, value))
        self.set_value(v)

        
    def __str__(self):
        vstr = format_decimal(self.value, self.SHOW_SCIENTIFIC_NOTATION)
        return "%s%s%s"%(vstr, self.multiplier_symbol, self.SYMBOLS[0] if self.SYMBOLS else (self.NAMES[0] if self.NAMES else ""))


    def __int__(self):
        return int(self.value)


    def __float__(self):
        return float(self.value)


    def __eq__(self, other):
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for ==: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        return self.value == v


    def __lt__(self, other):
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for <: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        return self.value < v


    def __gt__(self, other):
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for >: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        return self.value > v


    def __le__(self, other):
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for <=: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        return self.value <= v


    def __ge__(self, other):
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for >=: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        return self.value >= v


    def __abs__(self):
        r = type(self)(self)
        if r.value < 0:
            r.value *= -1
        return r
    

    def __add__(self, other):
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for +: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        r = type(self)(self)
        r.value += v
        return r


    def __iadd__(self, other):
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for +=: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        self.value += v
        return self


    def __sub__(self, other):
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for -: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        r = type(self)(self)
        r.value -= v
        return r


    def __isub__(self, other):
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for -=: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        self.value -= v
        return self


    def __mul__(self, other):
        if isinstance(other, Unit) and not type(self) is Unit:
            raise TypeError("unsupported operand type(s) for *: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for *: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        r = type(self)(self)
        r.value *= v
        return r


    def __imul__(self, other):
        if isinstance(other, Unit) and not type(self) is Unit:
            raise TypeError("unsupported operand type(s) for *=: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for *=: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        self.value *= v
        return self


    def __truediv__(self, other):
        if isinstance(other, Unit) and not type(self) is Unit:
            raise TypeError("unsupported operand type(s) for /: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for /: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        r = type(self)(self)
        r.value /= v
        return r


    def __itruediv__(self, other):
        if isinstance(other, Unit) and not type(self) is Unit:
            raise TypeError("unsupported operand type(s) for /=: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for /=: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        self.value /= v
        return self
    
            
    @property
    def context(self):
        return self._context if self._context else get_context()


    def convert(self, value):
        """
        Convert self to whatever the value might be.
        """
        if isinstance(value, (int, float, decimal.Decimal)) or (isinstance(value, type) and issubclass(value, (int, float, decimal.Decimal))):
            return Unit(self)
        elif isinstance(value, str):
            v = to_decimal(value)
            if v is None:
                u = self.context.unit(value)
                u.value = u.convert_value_from_instance(self)
                return u
            return v
        elif isinstance(value, type) and issubclass(value, Unit):
            return value(self)
        elif isinstance(value, Unit):
            u = type(value)(value)
            u.value = u.convert_value_from_instance(self)
            return u
        else:
            raise TypeError("unsupported type: %s"%(value.__name__ if isinstance(value, type) else type(value).__name__))


    def coerce(self, value, factor):
        """
        Coerce self to whatever the value might be by using a multiplying factor.
        NOTE: unit multipliers are in effect!
        """
        f = to_decimal(factor)
        if f is None:
            raise TypeError("expected factor argument of type int, float, Decimal or str, not %s"%type(factor).__name__)
        if f <= 0:
            raise TypeError("factor must be > 0: %s"%factor)
        if isinstance(value, (int, float, decimal.Decimal)):
            return Unit(self.value * f)
        elif isinstance(value, Unit):
            return type(value)((self.value * self.multiplier_value * f) / value.multiplier_value)
        elif isinstance(value, str):
            v = to_decimal(value)
            if v is None:
                u = self.context.unit(value)
                u.value = (self.value * self.multiplier_value * f) / u.multiplier_value
                return u
            else:
                return Unit(v)
        raise ValueError("could not coerce to value: %s"%value)
    
    
    def get_name(self, n=None):
        """
        Get the name of the unit based on it's value (singular or plural).
        """
        if not self.NAMES:
            if self.SYMBOLS:
                return self.SYMBOLS[0]
            return ""
        if n is None:
            n = self.value
        if n != 1 and len(self.NAMES) > 1:
            return self.NAMES[1]
        return self.NAMES[0]


    @property
    def name(self):
        """
        The name of the unit based on it's value (singular or plural).
        """
        return self.get_name()
    

    def get_value(self):
        """
        Get the value of the unit.
        """
        return to_decimal(self._value)


    def set_value(self, value):
        """
        Set the value of the unit.
        """
        v = to_decimal(value)
        if v is None:
            raise ValueError("invalid value: %s"%value)
        v = v.normalize()
        t = v.as_tuple()
        if t.exponent < 0 and len(t.digits) > abs(t.exponent):
            num_digits = 0
            digits = t.digits[t.exponent:][::-1]
            for i, dt in enumerate(digits):
                if dt != 0:
                    num_digits = abs(t.exponent) - i
                    break
            v = v.quantize(decimal.Decimal("1." + "0" * num_digits))
        self._value = v
        

    @property
    def value(self):
        """
        The value of the unit.
        """
        return self.get_value()


    @value.setter
    def value(self, value):
        self.set_value(value)


    def get_multiplier(self):
        """
        Get the symbol prefix of the unit.
        """
        return self._multiplier


    def set_multiplier(self, multiplier):
        """
        Set the symbol prefix of the unit.
        """
        if not multiplier or (isinstance(multiplier, str) and not multiplier.strip()):
            self._multiplier = None
            return
        index = self.find_multiplier(multiplier)
        if index < 0:
            raise ValueError("invalid multiplier: %s"%multiplier)
        self._multiplier = UnitMultiplier(self.MULTIPLIERS[index])


    @property
    def multiplier(self):
        """
        The symbol prefix of the unit.
        """
        return self.get_multiplier()


    @multiplier.setter
    def multiplier(self, multiplier):
        self.set_multiplier(multiplier)


    def get_multiplier_name(self, multiplier=None):
        """
        Return the multiplier name or an empty string for no multiplier.
        """
        if multiplier:
            use_multiplier = self.find_multiplier(multiplier)
            if not use_multiplier:
                raise ValueError("invalid multiplier: %s"%multiplier)
        else:
            use_multiplier = self.multiplier
        if use_multiplier:
            if use_multiplier.names:
                return use_multiplier.names[0]
            if use_multiplier.symbols:
                return use_multiplier.symbols[0]
        return ""


    @property
    def multiplier_name(self):
        """
        The name of the multiplier or an empty string for no multiplier.
        """
        return self.get_multiplier_name()
        

    @property
    def multiplier_value(self):
        if self._multiplier and self._multiplier.value:
            return self._multiplier.value
        return 1


    @property
    def multiplier_symbol(self):
        if self._multiplier:
            if self._multiplier.symbols:
                return self._multiplier.symbols[0]
            if self._multiplier.names:
                return self._multiplier.names[0]
        return ""
    

    def handle_match(self, match):
        """
        Gives the unit instance a chance to inspect the match and decide how to interpret it.
        Returns a decimal or None if the match is invalid.
        """
        self._match = match
        self.multiplier = self._match.multiplier
        return to_decimal(match.number)
    

    def convert_value_to_instance(self, instance):
        """
        Convert the value of self to whatever value the specified instance would be.
        Returns None if the conversion is unsupported.
        """
        if isinstance(instance, (int, float, decimal.Decimal)):
            return type(instance)(self.value)
        elif isinstance(instance, str):
            v = to_decimal(instance)
            if not v is None:
                return v
            return self.convert_value_from_instance(self.context.unit(instance))
        elif type(instance) is Unit:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        return None
        

    def convert_value_from_instance(self, instance):
        """
        Convert the value of the instance to the value of self.
        Returns None if the conversion is unsupported.
        """
        if isinstance(instance, (int, float, decimal.Decimal)):
            return to_decimal(instance)
        elif isinstance(instance, str):
            v = to_decimal(instance)
            if not v is None:
                return v
            return self.convert_value_from_instance(self.context.unit(instance))
        elif type(instance) is Unit:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif isinstance(instance, Unit):
            return instance.convert_value_to_instance(self)
        return None
    
    
class Context:
    """
    Keeps track of registered unit types.
    """


    def __init__(self, *unit_types, measure=None):
        """
        Initialize a new context.
        """
        self._unit_types = []
        types = self.assert_unit_types(*unit_types)
        self._unit_types += types


    def __len__(self):
        return len(self._unit_types)


    def __getitem__(self, index):
        return self._unit_types[index]


    def __setitem__(self, index, value):
        try:
            t = Unit.assert_type(value)
        except AssertionError as e:
            raise ValueError("invalid unit type: %s"%e)
        if self._unit_types[index] != t:
            if t in self._unit_types:
                raise ValueError("unit type already registered: %s"%t.__name__)
            self._unit_types[index] = t


    def __delitem__(self, index):
        del self._unit_types[index]
            

    def __iter__(self):
        return iter(self._unit_types)


    def __in__(self, value):
        return value in self._unit_types
    

    def __iadd__(self, value):
        types = self.assert_unit_types(value)
        self._unit_types += types
        return self


    def __add__(self, value):
        ret = Context(*self._unit_types)
        types = self.assert_unit_types(value)
        ret._unit_types += types
        return ret
            

    def append(self, *values):
        types = self.assert_unit_types(*values)
        self._unit_types += types


    def remove(self, value):
        try:
            t = Unit.assert_type(value)
        except AssertionError as e:
            raise ValueError("invalid unit type: %s"%e)
        self._unit_types.remove(t)


    def find(self, value):
        try:
            return self._unit_types.index(value)
        except ValueError:
            return -1
            
            
    def assert_unit_types(self, *unit_types):
        ret = []
        for obj in unit_types:
            if isinstance(obj, (list, tuple)):
                ret += self.assert_unit_types(*obj)
            elif isinstance(obj, types.ModuleType):
                if not hasattr(obj, "UNITS") or not isinstance(obj.UNITS, (list, tuple)):
                    raise ValueError("module does not contain a UNITS list symbol: %s"%obj)
                ret += self.assert_unit_types(*obj.UNITS)
            else:
                try:
                    t = Unit.assert_type(obj)
                except AssertionError as e:
                    raise ValueError("invalid unit type: %s"%e)
                if not obj in self._unit_types:
                    ret.append(t)
        return ret
        

    def match(self, value):
        if value is None or isinstance(value, (int, float, decimal.Decimal)) or (isinstance(value, str) and (not to_decimal(value) is None or not value.strip())):
            return Unit
        if not isinstance(value, str):
            raise TypeError("expected int, float, Decimal or str, not %s"%type(value).__name__)
        s = value.strip()
        for t in self._unit_types:
            m = t.match_string(s)
            if m and m.start == 0:
                return t
        return None



    def unit(self, s):
        """
        Instance a new unit.
        """
        match = self.match(s)
        if not match:
            raise ValueError("invalid unit: %s"%s)
        return match(s)


    def parse_calculation(self, s):
        """
        Instance a new unit.
        """
        if isinstance(s, (int, float, decimal.Decimal)):
            return Unit(s)
        elif isinstance(s, Unit):
            return type(s)(s)
        elif isinstance(s, str):
            s = s.replace(" ", "")
            v = to_decimal(s)
            if not v is None:
                return v
        else:
            raise TypeError("invalid type: %s"%s)
        
        
        
    
    
_DEFAULT_CONTEXT = Context()
_CONTEXT = _DEFAULT_CONTEXT


def get_context():
    return _CONTEXT


def set_context(context):
    if context is None:
        _CONTEXT = _DEFAULT_CONTEXT
    elif isinstance(context, Context):
        _CONTEXT = CONTEXT
    else:
        raise TypeError("invalid context: %s"%context)


def unit(spec):
    return get_context().unit(spec)


