# python-units

A small library for handling various units such as metres, gallons, bytes and what not.

# Examples:

```python
from units import register_units, unit as U

if __name__ == "__main__":
    register_units(imperial_us.UNITS, si.UNITS, computer.UNITS)
    print(U("1m") + "4yd")
    print(U("4lbs") + "5hg")
    print(U("1l") + U("2m3") + U("4gal"))
    print(U("1B") + U("2kilobytes") + U("16kbits"))
```